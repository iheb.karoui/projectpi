import { useState } from "react";
import { Link } from "react-router-dom";
import useAuth from "../hooks/useAuth";
import img1 from '../assets/img1.jpg';

export default function UserAvatar() {
    const { auth } = useAuth();
    const { setAuth } = useAuth();
    const [isDropdownOpen, setIsDropdownOpen] = useState(false);

    const username = auth?.fName + ' ' + auth?.lName;
    const email = auth?.email;

    const handleSignOut = () => {
        setAuth(null);
    };

    const toggleDropdown = () => {
        localStorage.removeItem("accessToken");
        setIsDropdownOpen(!isDropdownOpen);
    };

    return (
        <div className="relative">
            <img
                id="avatarButton"
                onClick={toggleDropdown}
                className="w-10 h-10 rounded-full cursor-pointer"
                src={img1}
                alt="User dropdown"
            />
            {isDropdownOpen && (
                <div
                    id="userDropdown"
                    className="z-10 absolute top-12 right-0 bg-white divide-y divide-gray-100 rounded-lg shadow w-44 dark:bg-gray-700 dark:divide-gray-600"
                >
                    <div className="px-4 py-3 text-sm text-gray-900 dark:text-white">
                        <div>{username}</div>
                        <div className="font-medium truncate">{email}</div>
                    </div>
                    <ul className="py-2 text-sm text-gray-700 dark:text-gray-200" aria-labelledby="avatarButton">
                        <li>
                            <Link to="/medicalHistory" className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Medical History</Link>
                        </li>
                        <li>
                            <Link to="settings" className="block px-4 py-2 hover:bg-gray-100 dark:hover:bg-gray-600 dark:hover:text-white">Settings</Link>
                        </li>
                    </ul>
                    <div className="py-1">
                        <button onClick={handleSignOut} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100 dark:hover:bg-gray-600 dark:text-gray-200 dark:hover:text-white">Sign out</button>
                    </div>
                </div>
            )}
        </div>
    );
}
