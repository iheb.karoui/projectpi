import { Link } from 'react-router-dom';
import Dropdown from './dropdown';
import {useResponsive} from '../hooks/useResponsive'
import UserAvatar from './userAvatar';
import useAuth from '../hooks/useAuth';
import { useEffect, useState } from 'react';

function NavBar() {
  const isMobile = useResponsive()
  const [isConnected, setIsConnected] = useState(false);
  const {auth} = useAuth();

  useEffect(()=>{
    if(auth.accessToken){
      setIsConnected(true)
    }else{
      setIsConnected(false)
    }
    console.log(auth);
  },[auth])

  return (
<nav className=" bg-white border-gray-200 dark:bg-gray-900 dark:border-gray-700 h-20 items-center">
  <div className="max-w-screen-xl flex flex-wrap items-center justify-between mx-auto p-4">
    <a href="#" className="flex items-center space-x-3 rtl:space-x-reverse">
        <span className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">Medical Clinic</span>
    </a>
   { isMobile && <div >
      <Dropdown/>
    </div>}
    { !isMobile && <div className=" w-full md:block md:w-auto" >
      <ul className="flex flex-col  items-center font-medium p-4 md:p-0 mt-4 border border-gray-100 rounded-lg bg-gray-500 md:space-x-8 rtl:space-x-reverse md:flex-row md:mt-0 md:border-0 md:bg-white dark:bg-gray-800 md:dark:bg-gray-900 dark:border-gray-700">
        <li>
          <Link to="/">
            <h1  className="block py-2 px-3 font-thin text-xl font-mono text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">Home</h1>
          </Link>
        </li>
        <li>
          <Link to="/services">
            <h1 className="block py-2 px-3 font-thin text-xl font-mono text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">Services & Departments</h1>
          </Link>
        </li>
        <li>
          <Link to="/about">
            <h1 className="block py-2 px-3 font-thin text-xl font-mono text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent"> About </h1>
          </Link>
        </li>
        <li>
          <Link to="/doctors">
            <h1 className="block py-2 px-3 font-thin text-xl font-mono text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent">Doctors</h1>
          </Link>
        </li>
        <li>
          <Link to="/request_appointment">
            <h1 className="block py-2 px-3 font-bold text-l font-mono text-gray-900 rounded hover:bg-gray-100 md:hover:bg-transparent md:border-0 md:hover:text-blue-700 md:p-0 dark:text-white md:dark:hover:text-blue-500 dark:hover:bg-gray-700 dark:hover:text-white md:dark:hover:bg-transparent border-b-2 border-blue-500">Request appointment</h1>
          </Link>
        </li>
        <li>
          {!isConnected && 
          <Link to="/login" className='flex items-center'>
            <button type="button" className="text-gray-900 font-mono font-bold hover:text-white border border-gray-800 hover:bg-gray-900 focus:ring-4 focus:outline-none focus:ring-gray-300  rounded-lg text-sm px-5 py-2.5 text-center me-2 mb-2 dark:border-gray-600 dark:text-gray-400 dark:hover:text-white dark:hover:bg-gray-600 dark:focus:ring-gray-800">Login</button>
          </Link>}
          {isConnected &&
            <UserAvatar/>
          }
        </li>
      </ul>
    </div>
      }
  </div>
</nav>

  );
}

export default NavBar;