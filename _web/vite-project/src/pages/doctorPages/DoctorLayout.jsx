import {Outlet} from 'react-router-dom';

const Layout = () => {
    return(
    <div>
        <h1 className='text-red-600 font-extrabold'>the access layout</h1>
        <Outlet/>
    </div>
    )
}
export default Layout