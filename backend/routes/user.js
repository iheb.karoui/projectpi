import express from 'express';
import {addDoctor, addPatient, login} from '../controllers/user.js'


const router = express.Router();

router
    .route('/login')
    .post(login);
router
    .route('/register/patient')
    .post(addPatient);
router
    .route('/register/doctor')
    .post(addDoctor);

export default router;